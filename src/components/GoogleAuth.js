import React from 'react';
import {Button} from 'semantic-ui-react';
import { signIn, signOut} from '../actions';
import {connect} from 'react-redux';

class GoogleAuth extends React.Component{


    componentDidMount(){
        window.gapi.load('client:auth2', () => {
            window.gapi.client.init({
                clientId: '180093022797-eh1tftr8o16ej4pik9hckdcn3ngeorv0.apps.googleusercontent.com',
                scope: 'email'
            }).then(() =>{
                this.auth = window.gapi.auth2.getAuthInstance();
                this.onAuthChange(this.auth.isSignedIn.get());
                this.auth.isSignedIn.listen(this.onAuthChange);
            });
        });
    }

    onAuthChange = (isSignedIn) => {
        if(isSignedIn){
            this.props.signIn(this.auth.currentUser.get().getId());
        } else {
            this.props.signOut();
        }
    }

    onSignInClick = ()=>{
        this.auth.signIn();
    }

    onSignOutClick = ()=>{
        this.auth.signOut();
    }

    renderAuthButton(){

        if(this.props.isSignedIn === null){
            return null;
        }else if(this.props.isSignedIn){
            return(
                    <Button circular color='red' onClick={this.onSignOutClick}>
                        <i className="google icon"/>
                        Sign Out
                    </Button>
                );
        }else{
            return(
                <Button circular color='green' onClick={this.onSignInClick}>
                    <i className="google icon"/>
                    Sign In
                </Button>
            );
        }

    }


    render(){
        return(
            <div>
                {this.renderAuthButton()}
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {isSignedIn:state.auth.isSignedIn}
}

export default connect(mapStateToProps, {signIn, signOut})(GoogleAuth);